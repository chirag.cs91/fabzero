# Multilayer Vectorization
Taking an image that has multiple images and discrete or finite number of colours (5 or 6 should be enough).

Since the image will be in raster image, we need to vectorize the images into separate layers.

Then we select path > trace bitmap > and then select multiple scans (as we have more than 2 colours in the image). Then we select colours in the drop down menu. 

![ink](./images/ink.png)

Then we change the background colour if the background colour resembles the colour in the image by selecting file > document properties > select a background colour 

Go to Filters > colour > colourize and select the black preview

![colour](./images/color.png)

Then click on the vectorized image and select ungroup.

Raise the speckles in the muliple scans to 25

![spec](./images/spec.png)

Then go to file > document propoerties and then select resize page to drawing or selection.

then select layers > add layers and select layers according to the no of scans used.

The click on the file and the move the selected group to layers

Then click a layer for alignment and draw borders for this. Here is the final image:
![panther](./images/pink.png)

# Half toning
Use GIMP, import any image that has a bright foreground and dark background or vice versa.Go to colours and select brightness and contrast and then increase the contrast of the image.
Go to filters > distort > newsprint and select your preference.

![pref](./images/queen.png)

The tutorial video [is here](https://www.youtube.com/watch?v=gjD04ZAQDng&list=PLKDpiLmgp6Esmd-bqNYZtjosZFaAtdgNE&index=9&t=11s)