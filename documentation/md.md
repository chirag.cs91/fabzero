The FF team asked us to keep a notebook to record evertything that has been taught to us till date, but since I have a very bad habit of losing all my notebooks digital software to keep the records work better for me. Fran and Sibu asked us to install Visual Studio code, and we were also introduced to markdown. For a guy who doesnt like writing notes manually, mark down was heaven and the installtion easy peasy. I just need to install an extension from visual studio code:

![Markdown installation](./images/extension.png)

As the final work needs to be posted in a HTML format, there are multiple online tools that convert markdown file to HTML in seconds. Hence, all the documention need not be written in HTML converter all over again. Markdown is very simple to use and the preview on the right side of the screen makes things easier to analyze, just like in the image below.

![Markdown preview](./images/md.png)


The cheat sheet for markdown script is in [this link](https://www.markdownguide.org/cheat-sheet/)

**The only drawback that Markdown has is, it does not have a spell checker**
