# set, go!
The first day was *"unlearning whatever I had learned"*. we were asked to install ubuntu (linux based OS), I had used linux previously but not so much in depth. I had always been a windows person, but with this program I might finally uninstall my windows OS after failing to do it for the past 10 years. The only problem I have with uninstalling windows is I require microsoft office to do all my professional work but hey I can always buy one more PC to do my office related work. For now I have installed WSL (Windows sub-system for Linux).

* Here is the link to [install wsl](https://www.groovypost.com/news/disney-plus-launches-in-hong-kong/ "WSL installtion")

We also signed up to:
* [fablabs.io](https://fablabs.io/)
* [Gitlab](https://gitlab.com/users/sign_in)

We were also taught with basic [linux commands](https://maker.pro/linux/tutorial/basic-linux-commands-for-beginners)

For the Fabzero program we downloaded the following softwares in no particular order with download links:
* Text editor platform: 
    * [Visual Studio Code](https://code.visualstudio.com/download) OR

    * [VS codium (open source)](https://vscodium.com/) OR
    * [Atom](https://atom.io) 

* PCB Design:
    * [KiCAD](https://www.kicad.org/download/)


* Video editing:
    * [Kdenlive](https://kdenlive.org/en/download/)


* Image editing:
    * [Gimp](https://www.gimp.org/downloads/)


* Vector image editing:
    * [Inkscape](https://inkscape.org/release/inkscape-1.1.1/)


* 3D CAD:
    * [FreeCAD](https://www.freecadweb.org/downloads.php)

* Programmatic 3D CAD:
    * [Openscad](https://openscad.org/downloads.html)

* Video transcoder:
    * [Handbrake](https://handbrake.fr/downloads.php)

* Screen shot tool:
    * [Flameshot](https://flameshot.org/#download )
    
