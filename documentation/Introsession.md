# First Session
The introductory session was held with the candidates from all the Fablabs, Fab Foundation consulaltants (Fran and Sibu), Director Royal Society for STEM (RSSTEM) and Director Druk Holding and Investments. The introductory session was initiated by both the directors present and the candidates were briefed on the larger vision of establishing 4 Fablabs at one go. 

Some of the vision were:
1. To provide impetus for STEM education
2. To work on building a knowledge based society
3. To create an innovation ecosystem
4. To solve grass root/community problems through technology

The following image shows the overall vision:

![alt text](./images/innotechs.png)
