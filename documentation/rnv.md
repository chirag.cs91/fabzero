# Screenshot tool

[Flame shot](https://flameshot.org/) is a screenshot tool that provides out of the box features compared to a default screenshot application in a winndows or a mac OS such as:
* Marking the required area with an arrow
* Inserting text within the screen shot
* Blurring the content that you dont want a viewer to see
* Marking a step by step process

# Imagemagick CLI
[This tool](https://imagemagick.org/index.php) can be used to create, edit, compose, or convert digital images. It can read and write images in a variety of formats (over 200) including PNG, JPEG, GIF, WebP, HEIC, SVG, PDF, DPX, EXR and TIFF. ImageMagick can resize, flip, mirror, rotate, distort, shear and transform images, adjust image colors, apply various special effects, or draw text, lines, polygons, ellipses and Bézier curves.

ImageMagick is free software delivered as a ready-to-run binary distribution or as source code that you may use, copy, modify, and distribute in both open and proprietary applications.

## Installation

```linux
sudo apt install imagemagick
```

To resize an image we use this script:
```linux
convert imagename.jpg -resize 640x640 anothername.jpg
```
Here is the example and you can see the difference in size:

![Image size](./images/fs.png)

and Yes I used flameshot for this :D

### modify 

you can modify the size of the image using this script:
```linux
mogrify -resize 640x640 *.jpg
```
![mogrify image](./images/mog.jpg)

We can also create a horizontal strip by combining two images using:
```linux 
convert imagename1.jpg imagename2.jpg -geometry x400 +append stripimage.jpg
```
Here is the result:

![stripimage](./images/stripimage.png)