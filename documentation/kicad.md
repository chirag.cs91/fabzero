# PCB design using kiCad

KiCad offers us the following features
### Schematic Editor
KiCad's Schematic Editor supports everything from the most basic schematic to a complex hierarchical design with hundreds of sheets. Create your own custom symbols or use some of the thousands found in the official KiCad library. Verify your design with integrated SPICE simulator and electrical rules checker.

### PCB Editor
KiCad's PCB Editor is approachable enough to make your first PCB design easy, and powerful enough for complex modern designs. A powerful interactive router and improved visualization and selection tools make layout tasks easier than ever.

#### 3D viewer
KiCad's 3D Viewer allows easy inspection of your PCB to check mechanical fit and to preview your finished product. A built-in raytracer with customizable lighting can create realistic images to show off your work.

You can download kiCad [here](https://www.kicad.org/download/)

A basic video tutorial for kiCad [is here](https://www.youtube.com/watch?v=-tN14xlWWmA&ab_channel=MakeStuff)

Go to File > Project > New Project > schematic> Add symbol >choose your components

The image of the first assignment is:

![project](./images/kicad.png)

Some of the short-cut keys that are useful:
1. a: *To add components*
1. r: *To rotate the component*
1. Ctrl+C: *Copy component*
1. Ctril+V: *Paste component*
1. m: *to move the component*
1. g: *move with the already designed circuit*
1. e: *to view properties*
1. v: *to change the value of a component*

The electronics module can be in various packages such as the following image:

![pack](./images/packages.jpg)

It depends upon the package that you want to use for your project, for tinkering through-hole package is recommended.

## Designing PCB boards

1. Go to Tools > assign footprints (it will display the components listed in the schematic).
1. Click on view the footprint in footprint viewer
1. Select the required footprint
1. View the selected component in 2D as well as in 3D

![](./images/fp.png)

Once done, 
1. Open PCB in board editor
1. Pull the component from the schematic (Click on update PCB with hanges made to schematic)

Make the board:

![board](./images/board.png)

Then route the connection:
* x : shortcut key to route the connection

To change the route setting, click route > then select interactive route setting

We can make the following changes:

![route](./images/irs.png) 

For production or fabricating the PCB we need to put the circuit with a border for cut out: for square, rectangle or circle we can do it directly from kiCAD while for custom shapes we can use [inkscape](./vector.md)

To make a border for cut out click on draw a line, then select edge cut layer.

The image with edge cut:

![edge](./images/brd.png)

For fablabs we make the line width of .8 mm (go to properties and then change the line width to 0.8 mm), *its because the width of the cutting tool in a Fablab is 0.8 mm.*  

For safe cutting it is better to select 8 mm as the size of the edge:

![safe](./images/mm.png)

You can view the circuit in 3D:
* click view > select 3D viewer

![3D](./images/3d.png)

## Manufacturing

In fablabs the PCB is fabricated via milling machines, but we wont be using milling machine to mill the entire circuit but only key components.

To start click on edit board setup then go to design rules, the design rules constraintscan be kept according to the image below::

![rule](./images/set.png)

then select pre-defined sizes and select the right size:

![sizes](./images/sizes.png)

Then click on Inspect > Design Rule Checker

Then go to File > Fabrication outputs > then select gerber

From the image below we can only select top front copper layer and edge cut as shown below:

![top](./images/top.png)

Then click on generate drill files.

Now we need to export the board into svg file. choose file > plot. In plot format select svg, select top copper and edge cut as above. then on the drill mark select actual size. Select plot edge cuts on all layers and select neative plot as well.

![plot](./images/plot.png)

Then go to the target folder and open the svg file in inkscape. Once in inkscape select the circuit and then go to file > document properties and then select resize page to the content.
Then click on file then export to png and make the dpi 1500 and export as follows.
![export](./images/export.png)


After the above, select etch cut and the export it as png as well. 

Finally select the top layer png and open it in GIMP, go to colors and invert colour and then save. We can also invert using [mods](http://modsproject.org/) as follows:

![](./images/inv.png)

## Double Layer PCB

Here is the link from Sibu on [double sided PCB](http://sibusaman.fabcloud.io/doublepcb/). In a fablab we use copper rivets (dimension is around 0.8 mm ) to connect top layer with the bottom layer. 
The battery= 3V and one LED takes 20mA of current, so 3 x 20mA= 60mA, and LED has forward voltage drop (3-5V), so the voltage at the resistor/diode will be 2V. Therefore, taking ohm's law V=IR, R=V/I, so we can take a 10 ohm resistor or even less just for protection to LED. 

Here is the schematic:
![sch](./images/sch.png)

We make changes to the board with double layer, we use *v* to use via for the connection from bottom layer to the top layer. To chage the path of the wire or via press *d*, it will move the whole path.

![clock](./images/clk.png)

The cost of PCB is dependant on: 
1. Area of the PCB
2. No of vias used
3. Type and no of components used

Then go to File > plot and save it as svg. Select drill marks as none so that the hole on the via isnt dispalyed.

![plot2](./images/plot2.png)

Convert the SVG file into ong via inkscape

![front cut](./images/frontcut.png)

Then open the the file in GIMP, and remove the edge cut by using the bucket tool.

Now we want the drill and the cut in the same layer, for this select the drill marks as *Actual* then plot. Open the saved svg in inkscape, then select the boundary and the then open in GIMP. Use the bucket tool for viewing the vias only as in the image below:

![via](./images/via.png)

Then go to kiCad and select the bottom layer in plot, in drill marks select none as the holes will be drilled from the drill and cut png file. The image generated for the bottom layer needs to be flipped as when we mill the board the orientation will change. For this go to Tools > transform > flip.

So first we mill the top layer, then the vias and then the bottom layers.

Here is the [video link](https://www.youtube.com/watch?v=IRs4qjiby7o&list=PLKDpiLmgp6Esmd-bqNYZtjosZFaAtdgNE&index=5) for the full tutorial

Here is the second [video link](https://www.youtube.com/watch?v=wiEoQZYIGHc&list=PLKDpiLmgp6Esmd-bqNYZtjosZFaAtdgNE&index=6)

Here is the third [video link](https://www.youtube.com/watch?v=9RzUj0LRGow&list=PLKDpiLmgp6Esmd-bqNYZtjosZFaAtdgNE&index=8)

Some of the books we can refer to for electronics design [is here](https://drive.google.com/drive/folders/1Av_mo4ZN92CwpM5Ewie05H2xLCndN7qi)



