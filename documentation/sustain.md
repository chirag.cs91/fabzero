# Designing any Innovation lab

This was presented from a document that Fran has compiled with his experience in operating *The Beach Lab* in Spain, all the images below have been extracted from the document. This document can be found in [this Github repo](https://github.com/TheBeachLab). 

We should start with a strategic plan of why we are creating the space and it should not be focussed on what are the machines that we want.

![why](./images/why.png)

The planning process of the lab:

![plan](./images/planning.png)

## An ideal layout:

Does not exist, so we need to make an ideal lab based on location, social and cultural background.

Principles and values:

![value](./images/princ.png)

There must be three separate spaces:
1. Learning space
2. Sharing space
3. working space


Keep all the discarded electronics items, one mans trash can be another man's bounty.

The spaces can be kept as:

![space](./images/spaces.png)

Some of the indoor amenities that can be made available are:

![spaces](./images/space.png)

While in the outdoor area we can have:

![out](./images/out.png)




