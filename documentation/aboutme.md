# Introduction
I am Chirag and I work with Druk Holding and Investments (DHI) Limited. We are on the verge of setting up only the third Super Fab Lab in the world, after *Massachhusetts Institute of Technology,USA* and *Super Fab Lab, Kerala*. This repository contains the purpose of saving the knowledge received from Fran and Sibu, who were delegated by Fab Foundation. Fab zero training is being conducted to gain hands-on training to operate and maintatin a Fablab. The candidates are from the following labs:
1. CNR, Lobeysa
2. JWPTI, Gelephu
3. Super Fab lab, Thimphu
4. The Royal Academy, Paro

the whole idea of having a git repo is to save the progress of the work and to reflect on the experience/techniques gained from the course. As the figure below shows that we can only retain 58.2% out of 100% in 20 mins. So trying to save everything.

The gitlab link for the full Fab zero course is [this](https://gitlab.fabcloud.org/fabzero/fabzero) 

Here is the list of all the session in daily order:
* [Introductory session](../documentation/Introsession.md)







