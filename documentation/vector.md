# Vector and Raster

Vector image on the left is made up of points and lines and appears smooth. Vector graphics are digital art that is rendered by a computer using a mathematical formula. Raster images are made up of tiny pixels, making them resolution dependent and best used for creating photos. This means that if you scale a vector image, it will maintain a smooth, un-pixelated appearance, whereas a raster image will become pixelated. Some differences between them:

![Vector and raster](./images/vr.png)

# Inkscape
YOu can download inkscape from [this link](https://inkscape.org/release/inkscape-1.1.1/)

The design process may begin by doodles on a napkin, a sketched mindmap, a photo of a memorable object, or a mockup in software which really wouldn't work to complete the project. Inkscape can take you from this stage to a final, professional-grade design format which is ready for publication on the web or in physical form. If you are new to the process of creating vector graphics it may feel different, but you will quickly be pleased by the flexibility, and power Inkscape offers.

Here is the Inkscape UI:

![](./images/isui.png)

If there is any background in your *Raster* image you can remove it using [this link](https://logosbynick.com/inkscape-how-to-remove-background/#:~:text=To%20use%20your%20newly%20drawn,subject%20and%20remove%20the%20background.)

In inkscape you can directly import an image by drag and drop. Then click on path then follow this process:

![process](./images/is.png)

Then adjust the brightness level and see the difference beween raster image and vector by zooming in, the resolution is quite clear:

![diff](./images/diff.png)

You can also compare with different levels and then choose the beast depending on the machine you want to choose to print it:

![brightness](./images/bri.png)

Then go to File and select document properties, then you can select resize page to the drawing to fit the image perfectly. The border line touch the image directly, so a bit of gap needs to be kept between the image and the border.
lock margins and then increase it so that there is ample space between image and the border:

![lock](./images/mar.png)


To convert vector image to svg, you can select File>export to png> and then change the dpi (according to requirement)

Note:  
*For printing in a standard printer 200-300 dpi should be good enough, for machines 600 dpi, for PCB 1200 should be great*

I worked with the above images as an assignment to learn inkscape.


Here is the [tutorial video](https://www.youtube.com/watch?v=8zLj4x8VnbY) from Fran

# Download youtube.com videos from terminal

We use [youtube-dl](https://github.com/ytdl-org/youtube-dl), it is a free and open source download manager for video and audio from YouTube and over 1,000 other video hosting websites. 

To install use:
```linux
sudo apt install youtube-dl
```

To download videos:
```linux
youtube-dl https://www.youtube.com/watch?v=csdgM2kDXUo
```

To check the quality of videos:
```linux
youtube-dl -F https://www.youtube.com/watch?v=csdgM2kDXUo
```

To select just audio from the files above:
```linux
youtube-dl -f 251 https://www.youtube.com/watch?v=csdgM2kDXUo
```



# Lossless Cut
Use losslesscut to edit video with only the required time slot. To install use: 
```linux
sudo apt get losslesscut
```
The GUI of losslesscut:

![llc](./images/llc.png)

To export: 

![exp](./images/exp.png)


Neil's bag of tricks http://academy.cba.mit.edu/classes/computer_design/video.html

Fran's bag of tricks https://github.com/TheBeachLab/myComputing/blob/master/doc/video.md