# Git 
Git is a tool for undertaking collaborative projects, keeping your work secure and it's features such as version management, project tracker, kanban board and lots of other features. 

To install Git in WSL use:
```git
sudo apt install git
```
Then we configure and register for Git using:
```git
cd ~/folder_name # replace this with your repository folder
git config --global user.name "yourname" #set name for commits
git config --global user.email "youremail" #set email for commits
git config --global push.default simple #default push only the current branch
git config --global core.editor nano #set the editor
```
Then we can clone the repository from our local system either via:
1. HTTP
2. SSH

This image shows the clone function:

![Clone image](./images/clone.jpeg)

The following is the basic git work flow:
```Git
cd ~/repositoryfolder     # go to the local repository folder
du -sh personalsubfolder  # check your subfolder size
git pull                  # pull other students changes
git add --all             # add your changes
git commit -m "message"   # write a meaningful message
git push                  # push to the archive
```

![Git work flow](./images/Gitvc.png)

I like to follow the ACP concept where you add, commit and then push, just like the following image:

![acp](./images/acp.jpg)

If you dont know him, his name is ACP pradyuman and he is an investigator from an Indian television series called CID which has been running for 20 years. 

Jokes aside! A full tutorial for how to setup can be [found here](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html)


## Git -as a project management tool

Git can also be a heaven for project managers. You can use the issues board to view the issues arising in the project as list, boards, services etc. You can also create project goals, milestones, countinous integration and deployment etc. 

![Project management](./images/pm.png)
