# fabzero 
**Fab zero** is a training conducted by *Fab Foundation,USA*. This training helps the Fablab Managers, Engineers and Technicians, to use all the tools that is required for the Fablab to function. The training is particularly focussed on giving hands on experience for installation, operation and maintenance of all the machines, tools and equipment. This training will also help the interested Fabbers to take up the Fab Academy course, as it serves as a foundational requirement to undertake the six month course.

This repository provides a step by step documentation of all the things that have been learned. The curriculum is based on the fabzero [gitrepo](https://gitlab.fabcloud.org/fabzero/fabzero/-/tree/master "fab zero repo") and the some of the youtube videos are in [this playlist](https://www.youtube.com/playlist?list=PLKDpiLmgp6Esmd-bqNYZtjosZFaAtdgNE)

If you want to know about me [click here](./documentation/aboutme.md)

Here is the daily session log:
* [Introductory session](./documentation/Introsession.md)
* [Installing WSL & Ubuntu, creating gitlab & fablabs.io account, open-source softwares with download links](./documentation/day1.md)
* [Mark down preview & tutorial](./documentation/md.md)
* [Git repository, git using ubuntu](./documentation/git.md) 
* [Flame shot & imagemagick](./documentation/rnv.md)
* [2D vector & raster, inkscape and video editing](./documentation/vector.md)
* [PCB design using kiCad](./documentation/kicad.md)
* [Operating, planning and managing any innovation space](./documentation/sustain.md)
* [Multilayer vectorization and halftoning](./documentation/half.md)







